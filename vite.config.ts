import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import { VitePluginFonts } from "vite-plugin-fonts";
import tsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig({
    base: "/creative-code",
    plugins: [
        react(),
        tsconfigPaths(),
        VitePluginFonts({
            google: {
                families: ["Work Sans"],
            },
        }),
    ],
});
