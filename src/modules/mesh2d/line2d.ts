import {
    crossVector2,
    scaleVector2,
    subtractVector2,
    translateVector2,
    Vector2,
} from "@/modules/mesh2d/vector2";

/**
 * A 2D line representation.
 */
export interface Line2D {
    /**
     * Some point on the line.
     */
    point: Vector2;
    /**
     * The direction of the line.
     */
    dir: Vector2;
    length?: number;
}

/**
 * Finds the intersection point of two lines. Returns undefined if none found.
 * Additionally, if line **a** has a length, the intersection point must be along
 * that length. Line **b**'s length is always taken to be infinite.
 *
 * @param a First line.
 * @param b Second line.
 * @returns The intersection point and its distance along the line
 * (or undefined if they don't intersect).
 */
export function intersectLines2D(
    a: Line2D,
    b: Line2D
): [position: Vector2, distance: number] | undefined {
    const dirCross = crossVector2(a.dir, b.dir);
    if (dirCross === 0) return undefined; // Parallel since have no perp.
    const delta = subtractVector2(b.point, a.point);
    const t = crossVector2(delta, b.dir) / dirCross;
    if (typeof a.length === "number" && (t < 0 || t > a.length))
        return undefined;
    return [translateVector2(a.point, scaleVector2(a.dir, t)), t];
}
