/**
 * Basic data structure for a vector with two components.
 */
export interface Vector2 {
    x: number;
    y: number;
}

/**
 * Scales a vector by a scalar value.
 *
 * @param v Input vector.
 * @param s Scalar factor.
 * @returns A scaled vector.
 */
export function scaleVector2<T extends Vector2 = Vector2>(v: T, s: number): T {
    return {
        ...v,
        x: v.x * s,
        y: v.y * s,
    };
}

/**
 * Translates the given vector by some amount.
 *
 * @param v The vector to translate.
 * @param delta The translation vector.
 * @returns A translated vector.
 */
export function translateVector2<T extends Vector2 = Vector2>(
    v: T,
    delta: Vector2
): T {
    return {
        ...v,
        x: v.x + delta.x,
        y: v.y + delta.y,
    };
}

/**
 * Subtracts a vector from another vector.
 *
 * @param v An input vector.
 * @param delta The vector to subtract.
 * @returns The result of subtraction.
 */
export function subtractVector2<T extends Vector2 = Vector2>(
    v: T,
    delta: Vector2
): T {
    return {
        ...v,
        x: v.x - delta.x,
        y: v.y - delta.y,
    };
}

/**
 * Calculates the length of a vector.
 *
 * @param v The input vector.
 * @returns The vectors length.
 */
export function lengthVector2<T extends Vector2 = Vector2>(v: T): number {
    return Math.sqrt(v.x * v.x + v.y * v.y);
}

/**
 * Normalises a vector.
 *
 * @param v The input vector.
 * @returns The normalised vector.
 */
export function normalizeVector2<T extends Vector2 = Vector2>(v: T): T {
    const length = lengthVector2(v);
    return {
        ...v,
        x: v.x / length,
        y: v.y / length,
    };
}

/**
 * Calculates the direction _from_ a vector _to_ another vector.
 *
 * @param from The starting vector.
 * @param to The vector being pointed at.
 * @returns The direction vector.
 */
export function directionVector2(from: Vector2, to: Vector2): Vector2 {
    return normalizeVector2(subtractVector2(to, from));
}

/**
 * Rotates the vector by 90degrees to find the perpendicular vector.
 *
 * @param v Input vector.
 * @returns The perpendicular vector.
 */
export function perpendicularVector2<T extends Vector2 = Vector2>(v: T): T {
    return {
        ...v,
        x: -v.y,
        y: v.x,
    };
}

/**
 * Rotates a vector.
 *
 * @param v Input vector.
 * @param theta The angle of rotation, in degrees.
 * @returns The result of rotation.
 */
export function rotateVector2<T extends Vector2 = Vector2>(
    v: T,
    theta: number
): T {
    return {
        ...v,
        x: v.x * Math.cos(theta) - v.y * Math.sin(theta),
        y: v.x * Math.sin(theta) + v.y * Math.cos(theta),
    };
}

/**
 * The dot product of two vectors.#
 *
 * @param a First vector.
 * @param b Second vector.
 * @returns The dot product.
 */
export function dotVector2(a: Vector2, b: Vector2): number {
    return a.x * b.x + a.y * b.y;
}

/**
 * The two-dimensional cross product. Finds the _magnitude_ of the vector
 * perpendicular to two vectors.
 *
 * @param a First vector.
 * @param b Second vector.
 * @returns The magnitude of the perpendicular vector.
 */
export function crossVector2(a: Vector2, b: Vector2): number {
    return a.x * b.y - a.y * b.x;
}

/**
 * Calculates the distance between to vectors.
 *
 * @param from First vector.
 * @param to Second vector.
 * @returns The distance between.
 */
export function distanceToVector2(from: Vector2, to: Vector2): number {
    return lengthVector2(subtractVector2(to, from));
}
