import { Face2D, faceToEdges2D } from "@/modules/mesh2d/face2d";
import { subtractVector2, Vector2 } from "@/modules/mesh2d/vector2";
import { Edge2D } from "@/modules/mesh2d/edge2d";

export interface BroadBounds2D {
    xMin: number;
    xMax: number;
    yMin: number;
    yMax: number;
}

export interface FaceBoundsPair2D {
    face: Face2D;
    bounds: BroadBounds2D;
}

export function getBoundsArea2D(bounds: BroadBounds2D) {
    return (bounds.xMax - bounds.xMin) * (bounds.yMax - bounds.yMin);
}

export function getFaceBroadBounds2D(face: Face2D): BroadBounds2D {
    const bounds = {
        xMin: Infinity,
        xMax: -Infinity,
        yMin: Infinity,
        yMax: -Infinity,
    };
    face.forEach((vertex) => {
        bounds.xMin = Math.min(bounds.xMin, vertex.x);
        bounds.xMax = Math.max(bounds.xMax, vertex.x);
        bounds.yMin = Math.min(bounds.yMin, vertex.y);
        bounds.yMax = Math.max(bounds.yMax, vertex.y);
    });
    return bounds;
}

export function isPointInBroadBounds2D(
    point: Vector2,
    bounds: BroadBounds2D
): boolean {
    return (
        point.x >= bounds.xMin &&
        point.x <= bounds.xMax &&
        point.y >= bounds.yMin &&
        point.y <= bounds.yMax
    );
}

export function isPointBetweenEdgeXBounds2D(
    point: Vector2,
    edge: Edge2D
): boolean {
    const [from, to] = edge;
    const minX = Math.min(from.x, to.x);
    const maxX = Math.max(from.x, to.x);
    return point.x >= minX && point.x <= maxX;
}

/**
 * Checks if a point is directly (meaning, not either side of) below an edge.
 *
 * @param point The point to check.
 * @param edge The edge to look below.
 * @returns Is the point below the edge?
 */
export function isPointDirectlyBelowEdge2D(
    point: Vector2,
    edge: Edge2D
): boolean {
    const [from, to] = edge;
    const maxY = Math.max(from.y, to.y);
    // Broad check
    if (!isPointBetweenEdgeXBounds2D(point, edge) || maxY < point.y) {
        return false;
    }

    // Vertical lines
    const delta = subtractVector2(from, to);
    if (delta.x === 0) return point.x === from.x && point.y <= maxY;

    // Otherwise
    const m = delta.y / delta.x;
    const y = m * (point.x - from.x) + from.y;
    return y >= point.y;
}

/**
 * Checks if the point is within the faces narrow bounds. That is, the point
 * lies within the face.
 *
 * Essentially uses a raycast along the y-axis from the very top of the face to
 * the point. The number of ray intersections is recorded. If the number is odd,
 * it means the point is within the face, otherwise it's outside.
 *
 * @param point A point to check.
 * @param face The face to check within.
 * @returns Is in bounds?
 */
export function isPointInFaceNarrowBounds2D(
    point: Vector2,
    face: Face2D
): boolean {
    let numBelow = 0;
    const edges = faceToEdges2D(face);
    edges.forEach((edge) => {
        if (isPointDirectlyBelowEdge2D(point, edge)) numBelow += 1;
    });
    return numBelow % 2 === 1;
}

/**
 * Checks if a point is within a face, using a broad then narrow check.
 *
 * @param point The point to check.
 * @param face The face to check within.
 * @param bounds The face's precalculated broad bounds.
 * @returns Is the point within the face?
 */
export function isPointInFaceBounds2D(
    point: Vector2,
    face: Face2D,
    bounds?: BroadBounds2D
): boolean {
    if (!isPointInBroadBounds2D(point, bounds ?? getFaceBroadBounds2D(face)))
        return false;
    return isPointInFaceNarrowBounds2D(point, face);
}
