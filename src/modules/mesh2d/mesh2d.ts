import { Face2D } from "@/modules/mesh2d/face2d";
import {
    BroadBounds2D,
    getBoundsArea2D,
    getFaceBroadBounds2D,
} from "@/modules/mesh2d/bounds2D";

/**
 * A complex face derivative, with more information and precalculated bounds.
 */
export interface Mesh2D<T = undefined> {
    face: Face2D;
    bounds: BroadBounds2D;
    area: number;
    data: T;
}

export function meshFromFace2D<T = undefined>(
    face: Face2D,
    data: T
): Mesh2D<T> {
    const bounds = getFaceBroadBounds2D(face);
    return {
        face,
        bounds,
        area: getBoundsArea2D(bounds),
        data,
    };
}

export function createRectangleMesh2D<T = undefined>(
    x: number,
    y: number,
    width: number,
    height: number,
    data: T
): Mesh2D<T> {
    const face: Face2D = [
        { x, y },
        { x: x + width, y },
        { x: x + width, y: y + height },
        { x, y: y + height },
    ];
    return meshFromFace2D<T>(face, data);
}
