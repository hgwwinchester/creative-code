import { Vector2 } from "@/modules/mesh2d/vector2";

export interface Vertex2D extends Vector2 {}
