import { Face2D } from "@/modules/mesh2d/face2d";
import {
    BroadBounds2D,
    isPointInFaceNarrowBounds2D,
} from "@/modules/mesh2d/bounds2D";
import { Vector2 } from "@/modules/mesh2d/vector2";

export function randomBroadBoundSamples2D(
    bounds: BroadBounds2D,
    samples: number = 50
): Vector2[] {
    const sampledPoints: Vector2[] = [];
    const xRange = bounds.xMax - bounds.xMin;
    const yRange = bounds.yMax - bounds.yMin;
    for (let i = 0; i < samples; i += 1) {
        sampledPoints.push({
            x: Math.random() * xRange + bounds.xMin,
            y: Math.random() * yRange + bounds.yMin,
        });
    }
    return sampledPoints;
}

export function randomFaceSamples2D(
    face: Face2D,
    bounds: BroadBounds2D,
    targetSamples: number = 50,
    maxSamples: number = 200
): Vector2[] {
    const sampledPoints: Vector2[] = [];
    let attempts = maxSamples;
    while (attempts > 0 && sampledPoints.length < targetSamples) {
        // Try to take all remaining samples in one go
        const samples = Math.min(
            attempts,
            targetSamples - sampledPoints.length
        );
        const broadSamples = randomBroadBoundSamples2D(bounds, samples);
        // Add samples that are within face
        // Don't do full bounds check because ALL pass broad check anyway
        broadSamples.forEach((sample) => {
            if (isPointInFaceNarrowBounds2D(sample, face))
                sampledPoints.push(sample);
        });
        attempts -= samples;
    }
    return sampledPoints;
}
