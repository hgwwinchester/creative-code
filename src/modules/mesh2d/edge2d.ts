import { Vertex2D } from "@/modules/mesh2d/vertex2d";
import { intersectLines2D, Line2D } from "@/modules/mesh2d/line2d";
import { directionVector2, distanceToVector2 } from "@/modules/mesh2d/vector2";

/**
 * Two connected vertices.
 */
export type Edge2D = [from: Vertex2D, to: Vertex2D];

/**
 * Cuts an edge with a line. Returns the resulting two edges if the cut is
 * intersects, or the single original edge if it doesn't.
 *
 * @param edge The edge to cut.
 * @param cut The line to cut it with.
 * @returns The resulting edges.
 */
export function cutEdge(
    edge: Edge2D,
    cut: Line2D
): [edgeA: Edge2D, edgeB?: Edge2D] {
    const edgeLine: Line2D = {
        point: edge[0],
        dir: directionVector2(edge[0], edge[1]),
        length: distanceToVector2(edge[0], edge[1]),
    };
    const intersection = intersectLines2D(edgeLine, cut);
    if (intersection) {
        const [point] = intersection;
        return [
            [edge[0], point],
            [point, edge[1]],
        ];
    }
    return [edge];
}
