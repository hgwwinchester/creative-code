import random from "random";
import { Vertex2D } from "@/modules/mesh2d/vertex2d";
import { cutEdge, Edge2D } from "@/modules/mesh2d/edge2d";
import { Line2D } from "@/modules/mesh2d/line2d";
import {
    directionVector2,
    distanceToVector2,
    lengthVector2,
    rotateVector2,
    scaleVector2,
    translateVector2,
    Vector2,
} from "@/modules/mesh2d/vector2";

/**
 * A clockwise loop of vertices.
 */
export type Face2D = Vertex2D[];

export function getFaceEdge2D(face: Face2D, index: number): Edge2D {
    return [face[index % face.length], face[(index + 1) % face.length]];
}

export function faceCenter2D(face: Face2D): Vector2 {
    let sum: Vector2 = {
        x: 0,
        y: 0,
    };
    face.forEach((vertex) => {
        sum = translateVector2(sum, vertex);
    });
    return scaleVector2(sum, 1 / face.length);
}

export function scaleFace2D(
    face: Face2D,
    scale: number,
    origin?: Vector2
): Face2D {
    const from = origin ?? faceCenter2D(face);
    return face.map((vertex) => {
        const dir = directionVector2(from, vertex);
        const dist = distanceToVector2(from, vertex);
        return translateVector2(from, scaleVector2(dir, dist * scale));
    });
}

export function faceToEdges2D(face: Face2D): Edge2D[] {
    const edges = [];
    for (let i = 0; i < face.length; i += 1) {
        edges.push(getFaceEdge2D(face, i));
    }
    return edges;
}

/**
 * Gets the index of the face's shortest edges.
 *
 * @param face The face.
 * @returns The index of the shortest edge, and its length.
 */
export function getShortestEdgeIndex2D(
    face: Face2D
): [index: number, length: number] {
    const edges = faceToEdges2D(face);
    let shortestIndex = 0;
    let shortestLength = distanceToVector2(edges[0][0], edges[0][1]);
    for (let i = 1; i < edges.length; i += 1) {
        const [from, to] = edges[i];
        const length = distanceToVector2(from, to);
        if (length < shortestLength) {
            shortestIndex = i;
            shortestLength = length;
        }
    }
    return [shortestIndex, shortestLength];
}

/**
 * Gets the index of the face's longest edges.
 *
 * @param face The face.
 * @returns The index of the longest edge, and its length.
 */
export function getLongestEdgeIndex2D(
    face: Face2D
): [index: number, length: number] {
    const edges = faceToEdges2D(face);
    let longestIndex = 0;
    let longestLength = distanceToVector2(edges[0][0], edges[0][1]);
    for (let i = 1; i < edges.length; i += 1) {
        const [from, to] = edges[i];
        const length = distanceToVector2(from, to);
        if (length > longestLength) {
            longestIndex = i;
            longestLength = length;
        }
    }
    return [longestIndex, longestLength];
}

export function cutFace(face: Face2D, cut: Line2D): Face2D[] {
    const faces: Face2D[] = [];
    let working: Face2D[] = [[]];
    let isOpen = false;
    // Cut each edge
    faceToEdges2D(face).forEach((edge) => {
        const cutEdges = cutEdge(edge, cut);
        // Not cut
        if (cutEdges.length === 1) {
            working[0].push(cutEdges[0][0]);
        }
        // Cut, new face
        else if (!isOpen) {
            isOpen = true;
            // Add intersection point to each
            const oldFace = [...working[0], ...cutEdges[0]];
            const newFace = [cutEdges[0][1]];
            working = [newFace, oldFace];
        }
        // Cut, close face
        else {
            isOpen = false;
            const newFace = [...working[0], ...cutEdges[0]];
            const oldFace = [...working[1], cutEdges[0][1]];
            working = [oldFace];
            faces.push(newFace);
        }
    });
    // Still open, but cutting finished, join faces to close
    if (isOpen) {
        const joinedFace = [...working[1], ...working[0]];
        faces.push(joinedFace);
    } else faces.push(working[0]);
    return faces;
}

type Range = [low: number, high: number];

export interface RandomFaceCutOptions {
    edgeCenter: number;
    edgeDistribution: () => number;
    edgeRange: Range;
    angleDistribution: () => number;
    angleRange: Range;
}

export const defaultRandomFaceCutOptions: RandomFaceCutOptions = {
    edgeCenter: 0.5,
    edgeDistribution: random.exponential(1.5),
    edgeRange: [0.3, 0.7],
    angleDistribution: random.normal(Math.PI / 2, Math.PI / 128),
    angleRange: [Math.PI / 12, (11 * Math.PI) / 12],
};

function clamp(x: number, range: Range) {
    const [low, high] = range;
    if (x < low) return low;
    if (x > high) return high;
    return x;
}

export function randomFaceCut(
    face: Face2D,
    index: number,
    options: RandomFaceCutOptions = defaultRandomFaceCutOptions
): Face2D[] {
    const [from, to] = getFaceEdge2D(face, index);
    const dir = directionVector2(from, to);
    const length = distanceToVector2(from, to);
    const distance = clamp(options.edgeDistribution(), options.edgeRange);
    const point = translateVector2(from, scaleVector2(dir, length * distance));
    const angle = clamp(options.angleDistribution(), options.angleRange);
    const cut = {
        point,
        dir: rotateVector2(dir, angle),
    };
    return cutFace(face, cut);
}
