import { createBrowserRouter } from "react-router-dom";
import Root from "@/pages/root/Root";
import BureauPage from "@/pages/bureau/BureauPage";
import PainterlyCompressionPage from "@/pages/painterly-compression/PainterlyCompressionPage";

export const routes = createBrowserRouter(
    [
        {
            path: "/",
            element: <Root />,
        },
        {
            path: "/bureau",
            element: <BureauPage />,
        },
        {
            path: "/compression",
            element: <PainterlyCompressionPage />,
        },
    ],
    { basename: "/creative-code" }
);
