import { Link, useLocation } from "react-router-dom";
import classNames from "classnames/bind";
import { useCallback } from "react";
import styles from "./header.module.css";

const cx = classNames.bind(styles);

interface HeaderNavLinkProps {
    href: string;
    text: string;
    active: boolean;
}

function HeaderNavLink({ href, text, active }: HeaderNavLinkProps) {
    return (
        <Link to={href} className={cx("link", { active })}>
            {text}
        </Link>
    );
}

interface HeaderNavProps {
    paths: { href: string; text: string }[];
    ariaLabel: string;
}

// Don't add to index
/**
 * @param {HeaderNavProps} props Component props.
 * @internal
 * @constructor
 */
export function HeaderNav({ paths, ariaLabel }: HeaderNavProps) {
    const location = useLocation();
    const onPath = useCallback(
        (path: string) => location.pathname === path,
        [location.pathname]
    );
    return (
        <nav className={cx("nav")} aria-label={ariaLabel}>
            {paths.map(({ href, text }) => (
                <HeaderNavLink
                    key={href}
                    href={href}
                    text={text}
                    active={onPath(href)}
                />
            ))}
        </nav>
    );
}
