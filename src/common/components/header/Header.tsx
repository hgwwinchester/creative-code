import classNames from "classnames/bind";
import { Link } from "react-router-dom";
import styles from "./header.module.css";
import { HeaderNav } from "@/common/components/header/HeaderNav";

const cx = classNames.bind(styles);

export interface HeaderProps {
    paths: { href: string; text: string }[];
    navigationAriaLabel: string;
}

export function Header({ paths, navigationAriaLabel }: HeaderProps) {
    return (
        <header className={cx("header")}>
            <div className={cx("info")}>
                <Link to="/" className={cx("logo")}>
                    <span className={cx("name")}>{"Creative Code"}</span>
                </Link>
            </div>
            <HeaderNav paths={paths} ariaLabel={navigationAriaLabel} />
        </header>
    );
}
