import { Link } from "react-router-dom";
import classNames from "classnames/bind";
import styles from "./sketchlink.module.css";

const cx = classNames.bind(styles);

export interface SketchLinkProps {
    href: string;
    src: string;
    alt: string;
    caption: string;
}

export function SketchLink({ href, src, alt, caption }: SketchLinkProps) {
    return (
        <Link to={href} className={cx("link")}>
            <figure className={cx("figure")}>
                <img className={cx("image")} src={src} alt={alt} />
                <figcaption className={cx("caption")}>{caption}</figcaption>
            </figure>
        </Link>
    );
}
