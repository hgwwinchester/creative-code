import { ReactNode } from "react";

export interface LayoutProps {
    children?: ReactNode;
}

export interface PageProps extends LayoutProps {
    title: string;
    paths: { href: string; text: string }[];
    navigationAriaLabel: string;
}
