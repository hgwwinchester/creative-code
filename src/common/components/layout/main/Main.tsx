import classNamesBind from "classnames/bind";
import classNames from "classnames";
import styles from "./main.module.css";
import { LayoutProps } from "@/common/components/layout/layout";

const cx = classNamesBind.bind(styles);

export interface MainProps extends LayoutProps {
    noPad?: boolean;
    noGap?: boolean;
    className?: string;
}

export function Main({ children, noGap, noPad, className }: MainProps) {
    return (
        <main
            className={classNames(
                className,
                cx("main", { gap: !noGap, pad: !noPad })
            )}
        >
            {children}
        </main>
    );
}
