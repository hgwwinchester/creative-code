import classNames from "classnames/bind";
import { Main } from "@/common/components/layout/main/Main";
import { Header } from "@/common/components/header/Header";
import { PageProps } from "@/common/components/layout/layout";
import styles from "./sketchpage.module.css";

const cx = classNames.bind(styles);

export function SketchPage({
    title,
    paths,
    navigationAriaLabel,
    children,
}: PageProps) {
    return (
        <>
            <Header paths={paths} navigationAriaLabel={navigationAriaLabel} />
            <Main>
                <h1>{title}</h1>
                <section className={cx("sketch")}>{children}</section>
            </Main>
        </>
    );
}
