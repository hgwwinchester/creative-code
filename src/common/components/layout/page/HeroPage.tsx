import classNames from "classnames/bind";
import { Header, Main, PageProps } from "@/components";
import styles from "./heropage.module.css";

const cx = classNames.bind(styles);

export function HeroPage({
    title,
    paths,
    navigationAriaLabel,
    children,
}: PageProps) {
    return (
        <>
            <Header paths={paths} navigationAriaLabel={navigationAriaLabel} />
            <Main className={cx("hero")}>
                <h1 className={cx("title")}>{title}</h1>
                {children}
            </Main>
        </>
    );
}
