export function clamp(x: number, low: number = 0, high: number = 1) {
    if (x < low) return low;
    if (x > high) return high;
    return x;
}
