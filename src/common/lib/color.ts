function rotateHues(colors: number[][]) {
    return [...colors].map((color) => [
        (color[0] + 180) % 360,
        color[1],
        color[2],
    ]);
}

function interquartileRange(colors: number[][], spread: number = 0.25) {
    const q1 = colors[Math.floor(spread * colors.length)];
    const q3 = colors[Math.floor((1 - spread) * colors.length)];
    return [q1, q3];
}

export interface ColorStatisticsHSL {
    medianByHue: number[];
    hueRange: number;
    lightRange: number;
}

export function colorStatisticsHSL(
    colors: number[][],
    spread: number = 0.25
): ColorStatisticsHSL {
    // Need to compare sorted against rotated, since hue is a "rotational" value
    const sorted = [...colors].sort((a, b) => a[0] - b[0]);
    const rotated = rotateHues(colors).sort((a, b) => a[0] - b[0]);

    const sortedIQR = interquartileRange(sorted, spread);
    const rotatedIQR = interquartileRange(rotated, spread);
    const sortedRange = sortedIQR[1][0] - sortedIQR[0][0];
    const rotatedRange = rotatedIQR[1][0] - rotatedIQR[0][0];

    // Also need lightness stats (same for both)
    const lightness = [...colors].sort((a, b) => a[2] - b[2]);
    const lightnessIQR = interquartileRange(lightness, spread);

    if (sortedRange < rotatedRange) {
        const median = sorted[Math.floor(0.5 * sorted.length)];
        return {
            medianByHue: median,
            hueRange: sortedRange,
            lightRange: lightnessIQR[1][2] - lightnessIQR[0][2],
        };
    }

    const median = rotated[Math.floor(0.5 * sorted.length)];
    median[0] = (median[0] + 180) % 360;
    return {
        medianByHue: median,
        hueRange: rotatedRange,
        lightRange: lightnessIQR[1][2] - lightnessIQR[0][2],
    };
}
