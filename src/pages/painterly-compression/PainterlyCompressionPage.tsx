import { useMemo } from "react";
import random from "random";
import { SketchPage } from "@/components";
import { headerRoutes } from "@/pages/header-routes";
import { PainterlyCompression } from "@/pages/painterly-compression/PainterlyCompression";
import {
    defaultRandomFaceCutOptions,
    RandomFaceCutOptions,
} from "@/modules/mesh2d";

export default function PainterlyCompressionPage() {
    const cutOptions: RandomFaceCutOptions = useMemo(
        () => ({
            ...defaultRandomFaceCutOptions,
            angleDistribution: random.normal(Math.PI / 2, Math.PI / 12),
        }),
        []
    );
    return (
        <SketchPage
            paths={headerRoutes}
            title="Painterly Compression"
            navigationAriaLabel="Navigation"
        >
            <p>The sketch may take a moment to load. Refresh to try again.</p>
            <PainterlyCompression
                randomCutOptions={cutOptions}
                minWidth={20}
                cutLoops={30}
                width={1920}
                height={2400}
                maxLightRange={2}
                maxHueRange={3}
                spread={0.1}
            />
        </SketchPage>
    );
}
