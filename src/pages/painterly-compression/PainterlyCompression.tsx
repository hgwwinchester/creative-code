import Sketch from "react-p5";
import p5Types from "p5";
import { MutableRefObject, useCallback, useRef } from "react";
import Color from "color";
import random from "random";
import womanImage from "@/assets/woman.jpg";
import {
    createRectangleMesh2D,
    defaultRandomFaceCutOptions,
    getLongestEdgeIndex2D,
    getShortestEdgeIndex2D,
    Mesh2D,
    meshFromFace2D,
    randomFaceCut,
    RandomFaceCutOptions,
    randomFaceSamples2D,
    scaleFace2D,
    translateVector2,
    Vector2,
} from "@/modules/mesh2d";
import { clamp, colorStatisticsHSL } from "@/lib";

export interface PainterlyCompressionOptions {
    background: string;
    width: number;
    height: number;
    src: string;
    randomCutOptions: RandomFaceCutOptions;
    targetSamples: number;
    maxSamples: number;
    minWidth: number;
    maxHueRange: number;
    maxLightRange: number;
    cutsPerLoop: number;
    cutLoops: number;
    drawsPerLoop: number;
    baseMeshConstructor: () => Mesh2D<PainterlyMeshData>;
    meshPickLambda: number;
    sortEvery: number;
    scale: number;
    spread: number;
}

export const defaultPainterlyCompressionOptions: PainterlyCompressionOptions = {
    baseMeshConstructor(): Mesh2D<PainterlyMeshData> {
        return createRectangleMesh2D(0, 0, 1920, 2400, {
            hueRange: 360,
            lightRange: 100,
        });
    },
    cutLoops: 50,
    cutsPerLoop: 50,
    drawsPerLoop: 20,
    maxHueRange: 2,
    maxLightRange: 2,
    maxSamples: 200,
    minWidth: 10,
    targetSamples: 50,
    background: "#f1f3f3",
    width: 1920,
    height: 2400,
    src: womanImage,
    randomCutOptions: defaultRandomFaceCutOptions,
    meshPickLambda: 6,
    sortEvery: 10,
    scale: 1.05,
    spread: 0.25,
};

interface PainterlyMeshData {
    color?: number[] | undefined;
    hueRange: number;
    lightRange: number;
}

function cutNewMeshes(
    meshes: Mesh2D<PainterlyMeshData>[],
    indexDistribution: () => number,
    cutOptions: RandomFaceCutOptions = defaultRandomFaceCutOptions
) {
    // Sample a random index from the distribution
    const indexSample = clamp(indexDistribution(), 0, 0.9);
    const index = Math.floor(indexSample * meshes.length);

    // Cut the face
    const { face, data } = meshes[index];
    const cutFaces = randomFaceCut(
        face,
        getLongestEdgeIndex2D(face)[0],
        cutOptions
    );

    // Handle the arrays
    meshes.splice(index, 1);
    return cutFaces.map((cutFace) =>
        meshFromFace2D<PainterlyMeshData>(cutFace, {
            color: data.color,
            hueRange: data.hueRange,
            lightRange: data.lightRange,
        })
    );
}

interface SampledColor extends Vector2 {
    color: number[];
}

function sampleMeshColors(
    mesh: Mesh2D<PainterlyMeshData>,
    image: p5Types.Image,
    scale: Vector2,
    targetSamples: number = 50,
    maxSamples: number = 200
): SampledColor[] {
    const samples = randomFaceSamples2D(
        mesh.face,
        mesh.bounds,
        targetSamples,
        maxSamples
    );
    return samples.map((sample) => ({
        ...sample,
        color: image.get(sample.x * scale.x, sample.y * scale.y),
    }));
}

function sampleColorStatistics(samples: SampledColor[], spread: number = 0.25) {
    const colors = samples.map((sample) =>
        Color.rgb(sample.color).hsl().array()
    );
    return colorStatisticsHSL(colors, spread);
}

function shouldContinueMeshCutting(
    mesh: Mesh2D<PainterlyMeshData>,
    image: p5Types.Image,
    scale: Vector2,
    maxHueRange: number = 15,
    maxLightRange: number = 15,
    minWidth: number = 10,
    targetSamples: number = 50,
    maxSamples: number = 200,
    spread: number = 0.25
) {
    const shortest = getShortestEdgeIndex2D(mesh.face)[1];
    if (shortest < minWidth) return false;
    const samples = sampleMeshColors(
        mesh,
        image,
        scale,
        targetSamples,
        maxSamples
    );
    const { medianByHue, hueRange, lightRange } = sampleColorStatistics(
        samples,
        spread
    );
    // eslint-disable-next-line no-param-reassign
    mesh.data = {
        color: medianByHue,
        hueRange,
        lightRange,
    };
    return !(hueRange <= maxHueRange && lightRange <= maxLightRange);
}

/**
 * Performs smart mesh cuts a given number of times.
 *
 * @param meshes The meshes to cut.
 * @param image Sampled image.
 * @param scale Image scale.
 * @param cuts Cuts to perform.
 * @param indexDistribution A function that returns a random value for
 * determining the edge index to cut from.
 * @param options Additional options.
 * @returns The "done" meshes (that have low enough deviance) and the "working"
 * meshes.
 */
function cutLoop(
    meshes: Mesh2D<PainterlyMeshData>[],
    image: p5Types.Image,
    scale: Vector2,
    cuts: number,
    indexDistribution: () => number,
    options: PainterlyCompressionOptions
) {
    const {
        randomCutOptions,
        targetSamples,
        maxSamples,
        minWidth,
        maxHueRange,
        maxLightRange,
        spread,
    } = options;
    const working: Mesh2D<PainterlyMeshData>[] = [...meshes];
    const done: Mesh2D<PainterlyMeshData>[] = [];
    for (let i = 0; i < cuts && working.length > 0; i += 1) {
        const cutMeshes = cutNewMeshes(
            working,
            indexDistribution,
            randomCutOptions
        );
        cutMeshes.forEach((mesh) => {
            if (
                shouldContinueMeshCutting(
                    mesh,
                    image,
                    scale,
                    maxHueRange,
                    maxLightRange,
                    minWidth,
                    targetSamples,
                    maxSamples,
                    spread
                )
            ) {
                working.push(mesh);
            } else {
                done.push(mesh);
            }
        });
    }
    return [done, working];
}

function getAllMeshColors(
    meshes: Mesh2D<PainterlyMeshData>[],
    image: p5Types.Image,
    scale: Vector2,
    targetSamples: number = 50,
    maxSamples: number = 50,
    spread: number = 0.25
) {
    meshes.forEach((mesh) => {
        if (mesh.data.color) return;
        const samples = sampleMeshColors(
            mesh,
            image,
            scale,
            targetSamples,
            maxSamples
        );
        const { medianByHue } = sampleColorStatistics(samples, spread);
        // eslint-disable-next-line no-param-reassign
        mesh.data.color = medianByHue;
    });
}

function drawMesh(
    p5: p5Types,
    width: number,
    height: number,
    mesh: Mesh2D<PainterlyMeshData>,
    scale: number
) {
    const color = Color.hsl(mesh.data.color ?? [0, 0, 0])
        .rgb()
        .hex();
    p5.fill(color);
    const face = scaleFace2D(mesh.face, scale);
    p5.beginShape();
    face.forEach((vertex) => {
        const { x, y } = translateVector2(vertex, {
            x: -width / 2,
            y: -height / 2,
        });
        p5.vertex(x, y);
    });
    p5.endShape();
}

function drawLoop(
    p5: p5Types,
    meshes: Mesh2D<PainterlyMeshData>[],
    image: p5Types.Image,
    scale: Vector2,
    index: number,
    draws: number,
    options: PainterlyCompressionOptions
) {
    const { targetSamples, maxSamples, width, height, spread } = options;
    getAllMeshColors(
        meshes.slice(index, index + draws),
        image,
        scale,
        targetSamples,
        maxSamples,
        spread
    );
    for (let i = index; i < index + draws && i < meshes.length; i += 1) {
        drawMesh(p5, width, height, meshes[i], options.scale);
    }
}

function getPreload(
    options: PainterlyCompressionOptions,
    image: MutableRefObject<p5Types.Image | undefined>
) {
    const { src } = options;
    return function preload(p5: p5Types) {
        // eslint-disable-next-line no-param-reassign
        image.current = p5.loadImage(src);
    };
}

function getSetup(
    options: PainterlyCompressionOptions,
    image: MutableRefObject<p5Types.Image | undefined>,
    scale: MutableRefObject<Vector2 | undefined>
) {
    const { background, width, height } = options;
    return function setup(p5: p5Types, canvasParentRef: Element) {
        if (canvasParentRef.children.length >= 1) return;
        p5.pixelDensity(1);
        p5.createCanvas(width, height, p5.WEBGL).parent(canvasParentRef);
        p5.background(background);
        p5.noStroke();
        p5.smooth();
        if (!image.current) return;
        const source = image.current;
        // eslint-disable-next-line no-param-reassign
        scale.current = {
            x: source.width / width,
            y: source.height / height,
        };
    };
}

function getDraw(
    options: PainterlyCompressionOptions,
    image: MutableRefObject<p5Types.Image | undefined>,
    scale: MutableRefObject<Vector2 | undefined>
) {
    const {
        baseMeshConstructor,
        cutsPerLoop,
        cutLoops,
        drawsPerLoop,
        meshPickLambda,
        background,
        sortEvery,
    } = options;
    const indexDistribution = random.exponential(meshPickLambda);

    let finished = false;
    let cutting = true;
    let drawing = false;

    let currentCutLoop = 0;
    let currentDrawLoop = 0;

    let working: Mesh2D<PainterlyMeshData>[] = [baseMeshConstructor()];
    let done: Mesh2D<PainterlyMeshData>[] = [];

    return function draw(p5: p5Types) {
        if (!image.current || !scale.current) return;
        if (cutting) {
            // Cut
            const [nowDone, stillWorking] = cutLoop(
                working,
                image.current,
                scale.current,
                cutsPerLoop,
                indexDistribution,
                options
            );
            working = stillWorking;
            done = [...done, ...nowDone];
            currentCutLoop += 1;
            cutting = currentCutLoop < cutLoops && working.length > 0;
            if (cutting && cutLoops % sortEvery === 0)
                working.sort((a, b) => b.data.lightRange - a.data.lightRange);
        } else if (!drawing) {
            done = [...done, ...working];
            drawing = true;
            p5.background(background);
        } else if (!finished) {
            // Draw
            drawLoop(
                p5,
                done,
                image.current,
                scale.current,
                currentDrawLoop * drawsPerLoop,
                drawsPerLoop,
                options
            );
            currentDrawLoop += 1;
            finished = done.length < currentDrawLoop * drawsPerLoop;
        }
    };
}

export function PainterlyCompression(
    props: Partial<PainterlyCompressionOptions>
) {
    const options: PainterlyCompressionOptions = {
        ...defaultPainterlyCompressionOptions,
        ...props,
    };
    const optionsString = JSON.stringify(options);

    const image = useRef<p5Types.Image>();
    const scale = useRef<Vector2>();

    const preload = useCallback(getPreload(options, image), [optionsString]);
    const setup = useCallback(getSetup(options, image, scale), [optionsString]);
    const draw = useCallback(getDraw(options, image, scale), [optionsString]);
    return <Sketch preload={preload} setup={setup} draw={draw} />;
}
