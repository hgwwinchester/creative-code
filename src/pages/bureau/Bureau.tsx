import Sketch from "react-p5";
import p5Types from "p5";
import random from "random";
import { useCallback } from "react";
import {
    createRectangleMesh2D,
    defaultRandomFaceCutOptions,
    Face2D,
    getLongestEdgeIndex2D,
    getShortestEdgeIndex2D,
    Mesh2D,
    meshFromFace2D,
    randomFaceCut,
    RandomFaceCutOptions,
    scaleFace2D,
    translateVector2,
} from "@/modules/mesh2d";
import { clamp } from "@/lib";

export interface BureauMeshData {
    color: string;
    accent: boolean;
}

export interface BureauOptions {
    colors: string[];
    minWidth: number;
    randomCutOptions: RandomFaceCutOptions;
    baseMeshConstructor: () => Mesh2D<BureauMeshData>;
    width: number;
    height: number;
    sortEvery: number;
    cuts: number;
    accentMinimum: number;
    accentDisplayMinimum: number;
    obscuredColor: string;
    backgroundColor: string;
    meshPickLambda: number;
    scale: number;
}

const defaultColors = ["#d63838", "#38a9d6"];

const defaultBureauOptions: BureauOptions = {
    colors: defaultColors,
    minWidth: 10,
    randomCutOptions: defaultRandomFaceCutOptions,
    baseMeshConstructor: () =>
        createRectangleMesh2D<BureauMeshData>(0, 0, 500, 500, {
            color: "#232729",
            accent: false,
        }),
    width: 500,
    height: 500,
    sortEvery: 150,
    cuts: 200,
    backgroundColor: "#f1f3f3",
    accentMinimum: 50,
    accentDisplayMinimum: 25,
    obscuredColor: "#232729",
    meshPickLambda: 2,
    scale: 0.9,
};

function drawFace(
    p5: p5Types,
    face: Face2D,
    accentColor: string,
    obscuredColor: string,
    accentDisplayMinimum: number,
    width: number,
    height: number
) {
    const longest = getLongestEdgeIndex2D(face)[1];
    if (longest <= accentDisplayMinimum) p5.fill(accentColor);
    else p5.fill(obscuredColor);
    p5.beginShape();
    face.forEach((vertex) => {
        const { x, y } = translateVector2(vertex, {
            x: -width / 2,
            y: -height / 2,
        });
        p5.vertex(x, y);
    });
    p5.endShape();
}

export function cutNewMeshes(
    meshes: Mesh2D<BureauMeshData>[],
    done: Mesh2D<BureauMeshData>[],
    indexDistribution: () => number,
    accentColors: string[],
    obscuredColor: string,
    accentMinimum: number,
    minLength: number = 10,
    cutOptions: RandomFaceCutOptions = defaultRandomFaceCutOptions
) {
    // Sample a random index from the distribution
    const indexSample = clamp(indexDistribution());
    const index = Math.floor(indexSample * (meshes.length - 1));

    // Cut the face
    const { face, data } = meshes[index];
    const cutFaces = randomFaceCut(
        face,
        getLongestEdgeIndex2D(face)[0],
        cutOptions
    );

    // Handle the arrays
    meshes.splice(index, 1);
    cutFaces.forEach((cutFace) => {
        const shortest = getShortestEdgeIndex2D(cutFace)[1];
        const color =
            !data.accent && shortest < accentMinimum
                ? accentColors[Math.floor(Math.random() * accentColors.length)]
                : data.color;
        const cutMesh = meshFromFace2D<BureauMeshData>(cutFace, {
            color,
            accent: shortest < accentMinimum,
        });
        if (shortest < minLength) done.push(cutMesh);
        else meshes.push(cutMesh);
    });
}

function getSetup(options: BureauOptions) {
    const {
        width,
        height,
        randomCutOptions,
        cuts,
        minWidth,
        sortEvery,
        colors,
        accentMinimum,
        accentDisplayMinimum,
        obscuredColor,
        backgroundColor,
        meshPickLambda,
        scale,
        baseMeshConstructor,
    } = options;
    return function setup(p5: p5Types, canvasParentRef: Element) {
        p5.pixelDensity(1);
        p5.createCanvas(width, height, p5.WEBGL).parent(canvasParentRef);
        p5.background(backgroundColor);
        p5.noStroke();
        p5.smooth();
        const indexDistribution = random.exponential(meshPickLambda);
        const meshes: Mesh2D<BureauMeshData>[] = [baseMeshConstructor()];
        const done: Mesh2D<BureauMeshData>[] = [];
        for (let i = 0; i < cuts && meshes.length > 0; i += 1) {
            cutNewMeshes(
                meshes,
                done,
                indexDistribution,
                colors,
                obscuredColor,
                accentMinimum,
                minWidth,
                randomCutOptions
            );
            // Expensive...
            if (i % sortEvery === 0) meshes.sort((a, b) => b.area - a.area);
        }
        [...done, ...meshes].forEach(({ face, data }) =>
            drawFace(
                p5,
                scaleFace2D(face, scale),
                data.color,
                obscuredColor,
                accentDisplayMinimum,
                width,
                height
            )
        );
    };
}

export default function Bureau(props: Partial<BureauOptions>) {
    const options: BureauOptions = { ...defaultBureauOptions, ...props };
    const setup = useCallback(getSetup(options), [JSON.stringify(options)]);
    return <Sketch setup={setup} />;
}
