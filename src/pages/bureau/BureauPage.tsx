import { useCallback } from "react";
import { SketchPage } from "@/components";
import Bureau, { BureauMeshData } from "@/pages/bureau/Bureau";
import { headerRoutes } from "@/pages/header-routes";
import { createRectangleMesh2D } from "@/modules/mesh2d";

export default function BureauPage() {
    const meshConstructor = useCallback(
        () =>
            createRectangleMesh2D<BureauMeshData>(0, 0, 1024, 1024, {
                color: "#232729",
                accent: false,
            }),
        []
    );
    return (
        <SketchPage
            paths={headerRoutes}
            title="Bureau"
            navigationAriaLabel="Navigation"
        >
            <p>Refresh to try again.</p>
            <Bureau
                width={1024}
                height={1024}
                baseMeshConstructor={meshConstructor}
                minWidth={25}
                accentMinimum={200}
                accentDisplayMinimum={80}
            />
        </SketchPage>
    );
}
