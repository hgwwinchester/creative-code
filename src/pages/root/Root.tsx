import classNames from "classnames/bind";
import { HeroPage, SketchLink } from "@/components";
import { headerRoutes } from "@/pages/header-routes";
import styles from "./root.module.css";
import bureauImage from "@/assets/bureau.png";
import painterlyImage from "@/assets/painterly.png";

const cx = classNames.bind(styles);

function Root() {
    return (
        <HeroPage
            title="Creative Code"
            paths={headerRoutes}
            navigationAriaLabel="Navigation"
        >
            <p>By Henry Winchester</p>
            <section className={cx("examples")}>
                <div className={cx("examples-grid")}>
                    <SketchLink
                        href="/bureau"
                        src={bureauImage}
                        alt="An example of the Bureau algorithm."
                        caption="Bureau"
                    />
                    <SketchLink
                        href="/compression"
                        src={painterlyImage}
                        alt="An example of the Painterly Compression algorithm."
                        caption="Painterly Compression"
                    />
                </div>
            </section>
        </HeroPage>
    );
}

export default Root;
